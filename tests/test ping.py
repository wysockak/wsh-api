import requests

base_url = 'http://18.184.234.77:8080'


def test_ping():
    ping_response = requests.get(f'{base_url}/ping')
    assert ping_response.status_code == 200
    assert ping_response.text == 'pong'



